from flask import Flask, render_template
app = Flask(__name__)

@app.route("/1.0")
def simple():
    return render_template("simple.html")

@app.route("/1.1")
def data():
    return render_template("data.html")

@app.route("/2.0")
def javascript():
    return render_template("javascript.html")

@app.route("/2.1")
def button():
    return render_template("button.html")

if __name__ == "__main__":
    app.run(host='0.0.0.0', port=80)
