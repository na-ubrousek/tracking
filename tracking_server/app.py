import base64
import logging, logging.config
from flask import Flask, Response, request
app = Flask(__name__)


@app.route('/pixel.gif')
def pixel():
    logger = logging.getLogger('tracking')
    
    collected_data = {}
    # simple
    collected_data['prohlizec'] = request.user_agent.platform + "/" + request.user_agent.browser
    collected_data['ip'] = request.headers.get('X-Forwarded-For', request.remote_addr)
    collected_data['url'] = request.referrer
    # data
    collected_data['url'] = request.args.get('path')
    collected_data['titulek'] = request.args.get('title')
    # javascript
    collected_data['referrer'] = request.args.get('referrer')
    collected_data['rozliseni'] = request.args.get('resolution')
    collected_data['click'] = request.args.get('click')
    collected_data['cas'] = request.args.get('time')


    logger.info(', '.join(key + ": " + value for key, value in collected_data.items() if value))

    # transparent 1x1 GIF, 43 bytes
    gif = base64.b64decode(
        'R0lGODlhAQABAIAAAP8AAP8AACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw==')
    return Response(gif, mimetype="image/gif")


if __name__ == "__main__":
    logging.config.fileConfig('logging.conf')
    app.run(host='0.0.0.0', port=100)
