# Na ubrousek Industries tracking
Abych pochopil, jak funguje online sledování, naprogramoval jsem jednoduchou verzi. 

Doprovodné video: https://youtu.be/wkyJauwpf3U

## Spuštění
Příklad funguje v `docker` a vyžaduje `docker-compose`. 
```
docker-compose up
```
Pak v prohlížeči:
```
http://localhost/1.0
```
